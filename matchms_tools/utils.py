from functools import reduce
import pandas as pd
from matchms_tools.spectrum import Spectrum


def merge_spectrums(spectrums):
    if isinstance(spectrums, dict):
        spectrums = spectrums.values()
    dfs = [
        pd.DataFrame({"intensities": sp.peaks.intensities}, index=sp.peaks.mz)
        for sp in spectrums
    ]
    df = reduce(
        lambda left, right: pd.merge(
            left, right, how="outer", left_index=True, right_index=True
        ),
        dfs,
    )
    cum = df.fillna(0).mean(axis=1).to_frame().reset_index()
    cum.columns = ["mz", "intensities"]
    cum_spectrum = Spectrum(
        mz=cum.mz.values,
        intensities=cum.intensities.values,
        metadata={"scans": "3", "pepmass": (667.186, 0)},
    )
    return cum_spectrum
